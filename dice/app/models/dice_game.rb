


require 'di'

class DiceGame
  attr_accessor :dice
  attr_accessor :firstRollTotal
  attr_accessor :winningRolls
  attr_accessor :losingRolls
  attr_accessor :rollTotal

  def initialize

    @dice = [di1 = Di.new, di2 = Di.new]
    @firstRollTotal = @dice[0].number + @dice[1].number
    @winningRolls = [7, 11]
    @losingRolls = [2, 3, 12]
    @rollTotal = @firstRollTotal
  end

  def roll_again
    @winningRolls = @firstRollTotal
    @losingRolls = 7

  end



end


