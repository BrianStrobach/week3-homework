class Di

attr_accessor :di_numbers
attr_accessor :number

  def initialize
  @di_numbers = [1,2,3,4,5,6]
  @number = @di_numbers.sample
  end

  def roll
    @number = @di_numbers.sample
  end


end

