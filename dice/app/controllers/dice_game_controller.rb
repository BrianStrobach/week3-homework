require 'dice_game'

class DiceGameController < ApplicationController
  def initialize
    @mygame = DiceGame.new
  end

  def start_new_game


    if(@mygame.winningRolls.include? @mygame.rollTotal)
      render "win"

    elsif(@mygame.losingRolls.include? @mygame.rollTotal)
      render "lose"

    else
      @mygame.losingRolls = 7
      @mygame.winningRolls = @mygame.firstRollTotal
      render "roll"
    end
  end

  def roll
    @mygame.firstRollTotal = params["first_roll_total"].to_i
    @mygame.winningRolls = @mygame.firstRollTotal
    @mygame.losingRolls = 7

    if(@mygame.winningRolls == @mygame.rollTotal)
      render "win"

    elsif(@mygame.losingRolls == @mygame.rollTotal )
      render "lose"

    else
      render "roll"
    end
  end


end

